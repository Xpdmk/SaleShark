<?php

namespace App;

use function GuzzleHttp\Promise\all;
use Illuminate\Database\Eloquent\Model;
require __DIR__.'/../vendor/autoload.php';
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
//setlocale(LC_NUMERIC,'fi');

class VerkkokauppaScraper
{
    private $urls = array(
        "Audio ja hifi" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=28a",
        "Kamerat" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=23a", //
        /*"Kodinkoneet" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=19a",
        "Koti ja valaistus" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=20a",
        "Laukut ja matkailu" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=56a",
        "Lelut" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=16a",
        "Musiikki" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=26a",
        "Oheislaitteet" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=3a",
        "Ohjelmistot" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=9a",
        "Palvelut" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=15a",
        "Pelit ja viihde" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=24a",
        "Pienkoneet" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=27a",
        "Puhelimet" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=22a",
        "Ruoka ja juoma" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=30a",
        "Tarvike ja toimisto" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=21a",
        "Tietokoneet" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=5a",
        "TV ja video" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=18a",
        "Vauvat ja perhe" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=55a",*/
        "Verkko" => "https://www.verkkokauppa.com/fi/search/?query=tarjoukset&category=10a"
    );
    private $cssSalePrice = ".price-tag-content__price-tag-price.price-tag-content__price-tag-price--current";
    private $cssOriginalPrice = ".price-tag-content__price-tag-price.price-tag-content__price-tag-price--original";
    private $cssProductName = ".heading-page.product__name-title";
    private $cssProductLink = ".data__list-product-link";
    private $cssProductBrand = ".product-share-details dd a";
    private $scraper_id;

    public function __construct() {
    	$this->scraper_id = config("default_values.scraper.id");
    }

    public function getOriginId() {
    	return $this->scraper_id;
    }

	private function scrape($urls, $cssSelector, $thingsToScrape)
    {
        $resultArray = array();
        foreach ($urls as $key => $url) {

            //echo 'URL'.json_encode($url).'<br>';
            $client = new Client();
            $guzzleClient = new GuzzleClient(array(
                'timeout' => 10,
            ));
            $client->setClient($guzzleClient);
            $crawler = $client->request('GET', $url);
            $output = $crawler->filter($cssSelector)->extract($thingsToScrape);
            array_push($resultArray,$output);
        }
        return $resultArray;
    }

    public function scrapeProductURLs()
    {
        $selectors = array($this->cssProductLink);
        $cssSelector = $this->createSelectorString($selectors);
        $thingsToScrape = 'href';
        $productUrls = $this->scrape($this->urls, $cssSelector, $thingsToScrape);
        array_walk($productUrls, function(&$productUrl, $key2) {
            array_walk($productUrl, function (&$value, $key) {
                $value = 'https://www.verkkokauppa.com' . $value;
            });
        });
        return $productUrls;
    }
    public function scrapeDetails($urls){
        $results = array();
        $selectors = array($this->cssProductBrand,
            $this->cssProductName,
            $this->cssSalePrice,
            $this->cssOriginalPrice);

        $cssSelector = $this->createSelectorString($selectors);
        $thingsToScrape = '_text';

        foreach ($urls as $url){
            $scrapeResult = $this->scrape($url, $cssSelector, $thingsToScrape);
            array_push($results,$scrapeResult);
        }
        return $results;
    }

    public function groupProductData($productsByCategory,$urlsByCategory){
        $groupedData = array();
        $categoryIndex = 0;
        foreach($productsByCategory as $products){

            $urls = $urlsByCategory[$categoryIndex];
            //echo json_encode($products).'<br>';
            if(sizeof($products)!=0){
                for($i = 0; $i<sizeof($products);$i++) {
                    $url = $urls[$i];
                    $product = $products[$i];
                    if(isset($product[0])){
                        $category = array_keys($this->urls)[$categoryIndex];
                        array_push($groupedData,$this->createProductJSON($product,$category,$url));
                    }
                }
            }
            $categoryIndex++;
        }
        echo json_encode($groupedData);
        return $groupedData;
    }

    private function createSelectorString($selectors)
    {
        $cssSelector = '';
        foreach ($selectors as $key => $selector) {
            $cssSelector = $cssSelector.$selector.',';
        }
        $cssSelector = rtrim($cssSelector,',');

        return $cssSelector;
    }

    public function stringToNumber($string){
        $string = str_replace(',', '.', $string);
        $string = str_replace(chr(194) . chr(160), '', $string);
        $string = str_replace(' ', '', $string);

        return is_numeric($string) ? $string : null;
    }
    private function validateFields($name,$origPrice,$sale,$category,$url,$brand){
        return is_string($name) &&
            is_numeric($origPrice) &&
            is_numeric($sale) &&
            is_string($category) &&
            is_string($url) &&
            is_string($brand) ? true : false;
    }
    public function createProductJSON($product, $category, $url){
        for($j = 0; $j<sizeof($product[$j]);$j++) {

            $name = $product[$j];
            $brand = $product[$j + 1];
            $origPrice = null;
            $sale = null;

            if(isset($product[$j + 2])){
                $origPrice = $product[$j + 2];
            }
            if (isset($product[$j + 3])) {
                $sale = $product[$j + 3];
            }

            $origPrice = $this->stringToNumber($origPrice);
            $sale = $this->stringToNumber($sale);
            if (!is_numeric($sale)) {
                $sale = $origPrice;
                $origPrice = 0;
            }

            $valid = $this->validateFields($name,$origPrice,$sale,$category,$url,$brand);
            $valid ? $saleDetails = array(
                'product_name' => $name,
                'original_price' => $origPrice,
                'price' => $sale,
                'category' => $category,
                'brand' => $brand,
                'link' => $url
            ) : $saleDetails = null;
        }
        return $saleDetails;
    }
}

set_time_limit(0 );
