<?php
/**
 * Created by PhpStorm.
 * User: royja
 * Date: 4.10.2017
 * Time: 10.05
 */

namespace App;

use App\Model\Product;
use App\Model\Seller;
use App\Model\Store;
use App\Model\Location;
use App\Model\Origin;
use App\Model\Sale;
use Symfony\Component\Routing\Exception\InvalidParameterException;


class VerkkokauppaImporter { // TODO: Finish converting class to static

	public static $SELLER_NAME = "Verkkokauppa.com";
	public static $STORE_DETAILS = [
		[
			"name" => "Helsingin myymälä",
			"location" => [
				"address" => "Tyynenmerenkatu 11",
				"postal_code" => "00220",
				"country" => "Finland"
			]
		],
		[
			"name" => "Oulun myymälä",
			"location" => [
				"address" => "Kaakkurinkulma 4",
				"postal_code" => "90410",
				"country" => "Finland"
			]
		],
		[
			"name" => "Pirkkalan myymälä",
			"location" => [
				"address" => "Saapastie 2",
				"postal_code" => "33950",
				"country" => "Finland"
			]
		]
	];


	private static function getStores() {
		$seller = self::getSeller();

		// Get store names and location data
		$storeNames = [];
		$locationData = [];
		foreach (self::$STORE_DETAILS as $infoStore) {
			array_push($storeNames, $infoStore["name"]);
			array_push($locationData, $infoStore["location"]);
		}

		// Get store objects by their store name
		$stores = Store::where([
			'store_name' => $storeNames
		])->get();
		if ($stores === null) { // No Stores for Verkkokauppa.com
			$locations = self::getLocations($locationData);
			$stores = [];
			for($i = 0; $i < sizeof($storeNames); $i++) {

				$store = new Store([
					"seller_id" => $seller,
					"name" => $storeNames[$i],
				]);
				$store->setRelation('location', $locations[$i]);
				$store->setRelation('seller', $seller);
				array_push($stores, $store);

			}
		} else if ($stores->count() < sizeof(self::$STORE_DETAILS)) { // Some stores missing
			// Find and create missing stores
			foreach (self::$STORE_DETAILS as $infoStore) {
				$storeMissing = true;
				foreach ($stores as $store) {
					if ($store->name === $infoStore["name"]) {
						$storeMissing = false;
						break;
					}
				}
				if ($storeMissing) {
					$location = self::getLocations([$infoStore["location"]])->first();
					$store = new Store([
						"store_name" => $infoStore["name"]
					]);
					$store->location_id =$location->location_id;
					$store->seller_id = $seller->seller_id;
					$store->save();
					$stores->push($store);
				}
			}
		}
		return $stores;
	}

	private static function getSeller() {
		$seller = Seller::where('seller_name', self::$SELLER_NAME)->get()->first();
		if ($seller === null) { // Seller object doesn't exist
			echo self::$SELLER_NAME;

			// Initialize seller
			$seller = new Seller([
				"seller_name" => self::$SELLER_NAME
			]);
			$seller->save();
		}
		return $seller;
	}

	private static function getLocations($infoLocations) {
		if (gettype($infoLocations) != "array") {
			throw new InvalidParameterException();
		}

		$locations = Location::where([$infoLocations])->get();
		if ($locations === null) { // No locations found
			foreach ($infoLocations as $infoLocation) {
				$location = new Location($infoLocation);
				$location->save();
				array_push($locations, $location);
			}
		} else if ($locations->count() < sizeof($infoLocations)) { // All locations missing
			// Find and create missing locations
			foreach ($infoLocations as $infoLocation) {
				$locationMissing = true;
				foreach($locations as $location) {
					if ($location->name === $infoLocation["name"]) {
						$locationMissing = false;
						break;
					}
				}
				if ($locationMissing) {
					$newLocation = new Location($infoLocation);
					$newLocation->save();
					$locations->push($newLocation);
				}
			}
		}

		return $locations;
	}

	private static function getOrigin(int $originId = -1) {
		if ($originId < 0) {
			// Get scraper origin if exists
			$origin = Origin::find(config("default_values.scraper.id"));

			if ($origin === null) {
				$origin = new Origin([
					"origin_id" => config("default_values.scraper.id"),
					"origin_name" => config("default_values.scraper.name")
				]);
				$origin->save();
			} else {
				$origin = $origin->get()->first();
			}
		} elseif ($originId >= 0) {
			// Get origin if exits
			$origin = Origin::find($originId);

			if (!is_object($origin)) { // Origin not found
				throw new \Exception("No origin with origin_id " + $originId + " found in database.");
			} else {
				$origin = $origin->get()->first();
			}
		}

		return $origin;
	}

	private static function reduce(array $data, array $fields) {
		$filteredData = [];
		foreach ($fields as $field) {
			if (isset($data[$field])) {
				$filteredData[$field] = $data[$field];
			}
		}
		return $filteredData;
	}

	public static function importProducts(array $data, int $originId = -1) {

		$origin = self::getOrigin($originId);

		$stores = self::getStores();

		$productFields = [
			"product_name",
			"category",
			"brand",
		];
		$saleFields = [
			"sale",
			"price",
			"original_price",
			"link",
			"amount"
		];


		foreach ($data as $dataSet) {
			$productDataSet = self::reduce(
				$dataSet,
				$productFields
			);
			$product = new Product($productDataSet);
			$product->save();

			$saleDataSet = self::reduce(
				$dataSet,
				$saleFields
			);

			foreach($stores as $store) {
				$saleDataSet["store_id"] = $store;
				$sale = new Sale($saleDataSet);
				$sale->product_id = $product->product_id;
				$sale->origin_id = $origin->origin_id;
				$sale->store_id = $store->store_id;
				$sale->save();
			}
		}
	}
}