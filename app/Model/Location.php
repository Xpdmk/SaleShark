<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $fillable = ['postal_code', 'address', 'country'];
    protected $primaryKey = "location_id";
    protected $table = "locations";
	public $timestamps = false;

	public function stores() {
		return $this->belongsTo("App\Model\Store", "store_id", "store_id");
	}
}
