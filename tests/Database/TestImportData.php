<?php
return [
	[
		'product_name' => "test name 1",
		'original_price' => 25.56,
		'price' => 20.00,
		'category' => "bathroom",
		'brand' => "Meile",
		'link' => "https://www.testsite.com"
	],
	[
		'product_name' => "test name 2",
		'original_price' => 76.943,
		'price' => 54.200000005,
		'category' => "tech",
		'brand' => "Nvidia",
	],
	[
		'product_name' => "test name 3",
		'price' => 77.00,
		'category' => "garden",
		'brand' => "Kivinen",
		'link' => "https://www.testsite2.com"
	]
];