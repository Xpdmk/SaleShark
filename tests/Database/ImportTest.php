<?php

namespace Tests\Feature\Database;

use Tests\TestCase;

use App\VerkkokauppaImporter;

class ImportTest extends TestCase
{

	private $testData;

	public function setUp() {
		parent::setUp();
		$this->testData = require(__DIR__."/TestImportData.php");
	}

	public function testVerkkokauppaImporterTest() {
		VerkkokauppaImporter::importProducts($this->testData);
	}

}