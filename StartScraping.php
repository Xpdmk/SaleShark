<?php
/**
 * Created by PhpStorm.
 * User: Otso
 * Date: 11.10.2017
 * Time: 10.01
 */

use App\VerkkokauppaScraper;
use App\VerkkokauppaImporter;

$VS = new VerkkokauppaScraper();
$productURLs = $VS->scrapeProductURLs();
$products = $VS->scrapeDetails($productURLs);
$productJSON = $VS->groupProductData($products,$productURLs);
VerkkokauppaImporter::importProducts($productJSON);
