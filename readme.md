# SaleShark -avoinrajapinta alennuksille
Ajatuksena on kerätä kauppojen verkkosivuilta tietoa heidän allennuksistaan tietyin väliajoin, ja tarjota kerättyjä tietoja rajapintamme kautta. Käyttäjän on myös mahdollista päivittää rajapintaamme alennuksia manuaalisesti, jos esimerkiksi löytyy sellainen, jota ei verkkosivuilla mainita.

Tällä hetkellä rajapintamme tukee ainoastaan Verkkokauppa.com:in alennuksia, sillä universaalia ”scraper” -luokkaa on vaikea tehdä sivustojen rakenteiden erilaisuuden vuoksi. Ongelmia tulee myös, jos sivu sisältää paljon javascriptiä, sillä PHP ei osaa käsitellä niitä. Alennukset keräämme CSS valitsijoiden perusteella, jotka täytyy etukäteen manuaalisesti määrittää, lisäksi tapa, jolla ”scraper” navigoi sivuja kovakoodataan jokaiselle kaupalle erikseen. Käytimme Goutte -nimistä scraper kirjastoa.

Tässä versiossa tietojen keräämistä ei ole vielä automatisoitu, ja myöskin vanhat tiedot tulee poistaa tietokannasta manuaalisesti.

## Rajapinta
Salesharkin rajapintaa voi käyttää siis alennusten hakemiseen, sekä viemiseen. Rajapinnan saa käyntiin komennolla `php artisan serve`

Tarkat api kutsut ovat seuraavat:

### GET
/sales, mikä tahansa sales -taulun kenttä kelpaa parametriksi.

Esimerkiksi:
/sales?product_id=38&store_id=10

Tulos:
```
{ 
   "data":[ 
        { 
         "sale_id":106,
         "price":"730",
         "origin_id":1,
         "product_id":38,
         "store_id":10,
         "amount":1,
         "original_price":"891",
         "link":"https://www.verkkokauppa.com/fi/product/27012/fddvt/Buffalo-LinkStation-441D-16-Tt-verkkolevypalvelin",
         "store":{ 
            "store_id":10,
            "store_name":"Helsingin myymälä",
            "location_id":4,
            "seller_id":2,
            "location":{ 
               "location_id":4,
               "postal_code":"00220",
               "address":"Tyynenmerenkatu 11",
               "country":"Finland"
            },
            "seller":{ 
               "seller_id":2,
               "seller_name":"Verkkokauppa.com"
            }
         }
    ]
}
```

Jos kentän nimen kirjoittaa väärin, tulee virheilmoitus.

### POST
/post, original_price, price, product_name, amount, category, brand, link.

Esimerkiksi:
/post?original_price=100&price=50,00&product_name=Kallen mätitahna&amount=2&category=Elintarvikkeet&brand=Atria

Ei palautusta

Ensisilmäyksellä saattaa näyttää siltä, että scrapetut ja postatut tuotteet tulevat kolminkertaisena tietokantaan, mutta tämä johtuu siitä, että Verkkokaupalla on kolme myymälää, joissa kaikissa on pääosin samat alennukset. Tulevaisuudessa, teemme niin, että samalla alennuksella voi olla monta myymälää.

## Rajapintaan kuulumattomat testisivut

**/scrape**

Käskee VerkkokauppaScraperia hakemaan Verkkokaupan sivuilta tuotteita tietokantaan.

**/gettaaja**

Täällä voi kokeilla tietojen hakemista ja viemistä rajapinnasta

**/maps**

Esimerkkisivu, joka käyttää rajapintaamme sekä google mapsia.

## Tekijät
Metropolia, ohjelmistotuotanto, tieto- ja viestintätekniikan opiskelijat

Otso Pohjola, Lauri Koskinen ja Roy Järvinen


